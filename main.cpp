//#pragma GCC diagnostic ignored "-Wc++11-extensions" 

#include <iostream>
// using namespace std;

using std::cout;
using std::free;

// TODO: Need to go deeper!

int f() {
  bool b = true;
  //  unsigned char *p = reinterpret_cast<unsigned char *>(&b);
  //  *p = 10;
  return static_cast<int>(b == 0);  // reading from b is now UB
}

int table[4] = {};
bool exists_in_table(int v) {
  for (int i = 0; i < 4; i++) {
    if (table[i] == v)
      return true;  // return true in one of the first 4 iterations or UB due to
                    // out-of-bounds access
  }
  return false;
}

class A {
 public:
  A() : a(0) {}
  int *a;
};

class B {
 public:
  int b = 456;
};

int main() {
  f();

  exists_in_table(5);

  int *p = (int *)std::malloc(sizeof(int));
  int *q = (int *)std::realloc(p, sizeof(int));
  //*p = 1; // UB access to a pointer that was passed to realloc
  *q = 2;

  if (p == q)  // UB access to a pointer that was passed to realloc
    cout << *p << *q << '\n';

  //  throw "The exception";

  free(q);

  A a;
  B b;
  a.a = new int(123);
  b.b = *a.a;

  intptr_t c{reinterpret_cast<intptr_t>(a.a)};
  a.a = reinterpret_cast<int *>(c);

  std::cout << *a.a << std::endl;
  delete a.a;
  std::cout << b.b << std::endl;
}

//// #include <stdlib.h>

//// void *p;

//// int foo() {
////   p = malloc(7);
////   p = 0; // The memory is leaked here.
////   return 0;
//// }
