## Static Analizers and Sanitizers

    clang-tidy main.cpp --
    clang++ -fsanitize=undefined,address -g main.cpp && ./a.out
    clang-format -dump-config -style=google